/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/


	
	//first function here:

function fullInfo() {
			let yourName = prompt("Enter your Full Name: ");
			let yourAge = prompt ("Enter your age: ");
			let location = prompt ("Enter your location: ");
			console.log ("Hello, " + yourName);
			console.log ("You are " + yourAge + " years old.");
			console.log ("You live in " + location);
};

fullInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function bands(bands) {
		console.log(bands)
		

		}
bands("1. 21 Pilots");
bands("2. Arctic Monkeys");
bands("3. Tame Impala");
bands("4. Eve");
bands("5. Asian Kung-Fu Generation");


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function movieList(movies) {
		console.log(movies)
}

function rating(rottenTomatoes){
				console.log(rottenTomatoes)
}

movieList("1. 500 Days of Summer");
rating("Rotten Tomatoes Rating: 85%")
movieList("2. Demon Slayer: Mugen Train");
rating("Rotten Tomatoes Rating: 98%")
movieList("3. Your Name");
rating("Rotten Tomatoes Rating: 98%")
movieList("4. 1408");
rating("Rotten Tomatoes Rating: 79%")
movieList("5. Parasite");
rating("Rotten Tomatoes Rating: 99%")



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}; 

printFriends();

//console.log(friend1);
//console.log(friend2);